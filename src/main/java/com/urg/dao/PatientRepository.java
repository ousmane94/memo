package com.urg.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.urg.Entites.Patient;

@CrossOrigin("*") 
@RepositoryRestResource
public interface PatientRepository extends CrudRepository<Patient, Integer>  {

	public  Patient findByNumerPatient(int idPatient);
	public  Patient findByNomPatient(String nomPatient);
	@Query("SELECT p FROM Patient p ORDER BY p.nomPatient ASC")
	public Page<Patient> finPage(Pageable pageable);

}
