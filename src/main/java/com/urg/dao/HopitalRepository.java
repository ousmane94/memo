package com.urg.dao;

import java.util.List;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.urg.Entites.Hopital;
@CrossOrigin("*") 
@RepositoryRestResource
public interface HopitalRepository extends CrudRepository<Hopital, Integer>  {
	
	public  Hopital findByIdHopital(int idHopital);
	public  Hopital findByNomHopital(String nomHopital);
	@Query("SELECT h FROM Hopital h ORDER BY h.nomHopital ASC")
    Page<Hopital> finPage(Pageable pageable);
	@RestResource(path = "/byNomHopital")
	@Query("select h from Hopital h where h.nomHopital like %:mc%")
	public  List<Hopital> findByNomHopitalContains(@Param("mc")String mc);
	@RestResource(path = "/byNomHopitalPage")
	public  Page<Hopital> findByNomHopitalContains(@Param("mc")String des , Pageable pageable);
	//@RestResource(path = "/bymotcle")
	//@Query("select h from Hopital h where  h.capacite= :x ")
	//List<Hopital> findByNomHopitalOrCapaciteContains (@Param("x")int x);


	




}
