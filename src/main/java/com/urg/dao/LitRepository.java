package com.urg.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.urg.Entites.Hospitalisation;
import com.urg.Entites.Lit;
import com.urg.Entites.Salle;

@CrossOrigin("*") 
@RepositoryRestResource
public interface LitRepository  extends CrudRepository<Lit, Integer>{
	
	public  Lit findByIdLit(int idLit);
	public  Lit findByNumeroLit(String numeroLit);
	public  Lit findByNumeroLitAndSalle(String numeroLit , Salle salle);
	@Query("SELECT l FROM Lit l ")
    Page<Lit> finPage(Pageable pageable);
	@RestResource(path = "/bynumeroLit")
	public  List<Lit> findByNumeroLitContains(@Param("mc")String numero);
	@RestResource(path = "/byetalit")
	public  List<Lit> findByEtatContains(@Param("mc")String etat);
	@RestResource(path = "/bynumeroLitPage")
	public  Page<Lit> findByNumeroLitContains(@Param("mc")String numero , Pageable pageable);
	@Query("select l.hospitalisations from Lit l where l.numeroLit= :x")
	public  List<Hospitalisation>  findLitHospitalisations(@Param("x") String x);


}
