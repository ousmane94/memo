package com.urg.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.urg.Entites.Hopital;
import com.urg.Entites.Lit;
import com.urg.Entites.Salle;
import com.urg.Entites.Service;

@CrossOrigin("*") 
@RepositoryRestResource
public interface SalleRepository extends CrudRepository<Salle, Integer> {
	
	public Salle  findByIdSalle(int idSalle);
	public  Salle findByNomSalle(String nomSalle);
	@Query("SELECT s FROM Salle s ORDER BY s.nomSalle ASC")
    Page<Salle> finPage(Pageable pageable);
	@RestResource(path = "/byNomSalle")
	public  List<Salle> findByNomSalleContains(@Param("mc")String des);
	@RestResource(path = "/byNomSallePage")
	public  Page<Salle> findByNomSalleContains(@Param("mc")String des , Pageable pageable);
	@Query("select s.lits from Salle s where s.nomSalle= :x")
	public  List<Lit>  findLitSalles(@Param("x") String x);	
	public  List<Salle> findByService(Service s);
	public  List<Salle> findByNomSalleOrLocalisationOrTypeSalle(String nomservice,String localisation,String typeSalle);
	//@RestResource(path = "/bymclSalle")
	//public  List<Salle> findByNomSalleOrLocalisationOrTypeSalleContains(@Param("mc")String des,@Param("mc")String loc,@Param("mc")String typ);

}
