package com.urg.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import com.urg.Entites.Specialite;

public interface SpecialiteRepository extends CrudRepository<Specialite, Integer> {

	public Specialite  findByIdSpecialite(int idSpecialite);
	//@Query("SELECT s FROM Service s ORDER BY s.nomService ASC")
    //Page<Service> finPage(Pageable pageable);
	@RestResource(path = "/byNomService")
	public  List<Specialite> findByNomSpecialiteContains(@Param("mc")String des);
	//@RestResource(path = "/byNomServicePage")
	//public  Page<Specialite> findByNomServiceContains(@Param("mc")String des , Pageable pageable);
	//@Query("select s.salles from Service s where s.nomService= :x")
	//public  List<Specialite>  findSalleServices(@Param("x") String x);
	//@Query("select s from Service s where s.nomService= :x and  s.hopital=:y")
	public  Specialite findByNomSpecialite(String nomSpecialite);
	//public  List<Specialite> findByHopi(Hopital y);
	//public  Service findByNomService(String nomService);
}
