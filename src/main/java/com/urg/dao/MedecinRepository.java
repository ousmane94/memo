package com.urg.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.urg.Entites.Medecin;
import com.urg.Entites.Specialite;

public interface MedecinRepository extends CrudRepository<Medecin,Integer> {
	
	public Medecin findBymatricule(String matricule);
	public Medecin findByIdPersonne(Integer idPersonne);
	//@Query( "SELECT u FROM Utilisateur u ORDER BY  u.nom  where u.hopital like %:nom% " )
	//public List<Utilisateur> findByIdHopitals(@Param ("nomopital") String nomopital);
	@Query("SELECT u FROM Medecin u ORDER BY u.nom ASC")
    Page<Medecin> finPage(Pageable pageable);
	@Query("select u from Medecin u where u.username= :x and u.password=:y" )
	public  Medecin  findByUsernameAndPassword(@Param("x") String x ,@Param("y") String y );
	public  List<Medecin> findBySpecialite(Specialite y);
	public List<Medecin> findByAdresseOrNomOrPrenomOrProfilsOrPasswordOrUsernameOrMatricule(String adre,String nom,String prenom ,String profil, String pass, String user,String matricule);


}
