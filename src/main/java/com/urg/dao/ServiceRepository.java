package com.urg.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.urg.Entites.Hopital;
import com.urg.Entites.Service;
@CrossOrigin("*") 
@RepositoryRestResource
public interface ServiceRepository<Int>  extends CrudRepository<Service, Integer>{
	
	public Service  findByIdService(int idService);
	@Query("SELECT s FROM Service s ORDER BY s.nomService ASC")
    Page<Service> finPage(Pageable pageable);
	@RestResource(path = "/byNomService")
	public  List<Service> findByNomServiceContains(@Param("mc")String des);
	@RestResource(path = "/byNomServicePage")
	public  Page<Service> findByNomServiceContains(@Param("mc")String des , Pageable pageable);
	@Query("select s.salles from Service s where s.nomService= :x")
	public  List<Service>  findSalleServices(@Param("x") String x);
	//@Query("select s from Service s where s.nomService= :x and  s.hopital=:y")
	public  Service findByNomServiceAndHopital (String x ,Hopital y);
	public  List<Service> findByHopital(Hopital y);
	public  Service findByNomService(String nomService);


	


}
