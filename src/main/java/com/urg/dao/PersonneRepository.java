package com.urg.dao;

import org.springframework.data.repository.CrudRepository;

import com.urg.Entites.Personne;

public interface PersonneRepository extends CrudRepository<Personne, Integer> {

	Personne findByUsername(String username);

}
