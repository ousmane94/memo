package com.urg.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.urg.Entites.Hopital;
import com.urg.Entites.Personne;
import com.urg.Entites.Utilisateur;
@CrossOrigin("*") 
@RepositoryRestResource
public interface UtilisateurRepository extends CrudRepository<Utilisateur, Integer> {
	
	public Utilisateur findByUsername(String username);
	public Utilisateur findByIdPersonne(Integer idutilisateur);
	@Query("SELECT u FROM  Utilisateur u  ORDER BY u.nom ASC ")
	public  List<Utilisateur> findlisteUtilisateurs();

	//@Query( "SELECT u FROM Utilisateur u ORDER BY  u.nom  where u.hopital like %:nom% " )
	//public List<Utilisateur> findByIdHopitals(@Param ("nomopital") String nomopital);
	
	@Query("select h.utilisateurs from Hopital h where h.idHopital= :x")
	public  List<Utilisateur>  findUtilisateurs(@Param("x") int x);
	@Query("SELECT u FROM Personne u ORDER BY u.nom ASC")
    Page<Utilisateur> finPage(Pageable pageable);
	@Query("select u from Personne u where u.username= :x and u.password=:y" )
	public  Utilisateur  findByUsernameAndPassword(@Param("x") String x ,@Param("y") String y );
	public  List<Utilisateur> findByHopital(Hopital y);
	public List<Utilisateur> findByAdresseOrNomOrPrenomOrProfilsOrPasswordOrUsername(String adre,String nom,String prenom ,String profil, String pass, String user);

	
	

}
