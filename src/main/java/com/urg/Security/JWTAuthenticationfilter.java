package com.urg.Security;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.urg.Entites.Utilisateur;

public class JWTAuthenticationfilter extends UsernamePasswordAuthenticationFilter {
	private AuthenticationManager authenticationManager;
	
	
	public JWTAuthenticationfilter(AuthenticationManager authenticationManager) {
		super();
		this.authenticationManager = authenticationManager;
	}
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		Utilisateur utilisateur=null;
		try {
			utilisateur=new ObjectMapper().readValue(request.getInputStream(), Utilisateur.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		System.out.println("**************************");
		System.out.println("Username: "+ utilisateur.getUsername());
		System.out.println("password: "+ utilisateur.getPassword());
		return authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						utilisateur.getUsername(),
						utilisateur.getPassword()));
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		/*
		 * User springUser=(User) authResult.getPrincipal(); String
		 * jwt=Jwts.builder().setSubject(springUser.getUsername()) .setExpiration(new
		 * Date(System.currentTimeMillis()+SecurityContants.EXPIRATION_TIME))
		 * .signWith(SignatureAlgorithm.HS256, SecurityContants.SECRET) .claim("roles",
		 * springUser.getAuthorities()) .compact();
		 * response.addHeader(SecurityContants.HEADER_STRING,SecurityContants.
		 * TOKEN_PREFIX+jwt);
		 */
	    User springUser=(User)authResult.getPrincipal(); 
	    List<String> roles = new ArrayList<>();
	    springUser.getAuthorities().forEach(a ->{ 
	    	roles.add(a.getAuthority()); 
	    });
	    String jwt= JWT.create() .withIssuer(request.getRequestURI())
	    		  .withSubject(springUser.getUsername())
	    		  .withArrayClaim("profil",roles.toArray(new String[roles.size()]))
	    		  .withExpiresAt(new Date(System.currentTimeMillis()+SecurityContants.EXPIRATION_TIME))
	    		  .sign(Algorithm.HMAC256(SecurityContants.SECRET));
	   response.addHeader(SecurityContants.HEADER_STRING,SecurityContants.TOKEN_PREFIX+jwt);
	    
	}
	
	
 
}
/*
 * package com.urg.Security;
 * 
 * import java.io.IOException; import java.util.*;
 * 
 * import javax.servlet.FilterChain; import javax.servlet.ServletException;
 * import javax.servlet.http.HttpServletRequest; import
 * javax.servlet.http.HttpServletResponse;
 * 
 * import org.springframework.security.authentication.AuthenticationManager;
 * import org.springframework.security.authentication.
 * UsernamePasswordAuthenticationToken; import
 * org.springframework.security.core.Authentication; import
 * org.springframework.security.core.AuthenticationException; import
 * org.springframework.security.core.userdetails.User; import
 * org.springframework.security.web.authentication.
 * UsernamePasswordAuthenticationFilter;
 * 
 * import com.auth0.jwt.JWT; import com.auth0.jwt.algorithms.Algorithm; import
 * com.fasterxml.jackson.databind.ObjectMapper; import
 * com.urg.Entites.Utilisateur;
 * 
 * public class JWTAuthenticationFilter extends
 * UsernamePasswordAuthenticationFilter { private AuthenticationManager
 * authenticationManager; public JWTAuthenticationFilter(AuthenticationManager
 * authenticationManager) { super(); this.authenticationManager =
 * authenticationManager; }
 * 
 * @Override public Authentication attemptAuthentication(HttpServletRequest
 * request, HttpServletResponse response) throws AuthenticationException {
 * Utilisateur user=null; try { user = new
 * ObjectMapper().readValue(request.getInputStream(), Utilisateur.class); }
 * catch (Exception e) { throw new RuntimeException(e); } return
 * authenticationManager.authenticate( new UsernamePasswordAuthenticationToken(
 * user.getUsername(), user.getPassword() )); }
 * 
 * @Override protected void successfulAuthentication(HttpServletRequest request,
 * HttpServletResponse response, FilterChain chain, Authentication authResult)
 * throws IOException, ServletException {
 * 
 * User springUser=(User)authResult.getPrincipal(); List<String> roles = new
 * ArrayList<>(); springUser.getAuthorities().forEach(a ->{
 * roles.add(a.getAuthority()); }); String jwt= JWT.create()
 * .withIssuer(request.getRequestURI()) .withSubject(springUser.getUsername())
 * .withArrayClaim("profil",roles.toArray(new String[roles.size()]))
 * .withExpiresAt(new
 * Date(System.currentTimeMillis()+SecurityConstants.EXPIRATION_TIME))
 * .sign(Algorithm.HMAC256(SecurityConstants.SECRET));
 * response.addHeader(SecurityConstants.HEADER_STRING,SecurityConstants.
 * TOKEN_PREFIX+jwt);
 * 
 * }
 * 
 * 
 * 
 * }
 */
