package com.urg.Security;

public class SecurityContants {
	public static final String SECRET="ous@barry.net";
	public static final long EXPIRATION_TIME=10*72*3600;
	public static final String TOKEN_PREFIX="Bearer ";
	public static final String HEADER_STRING="Authorization";
}
