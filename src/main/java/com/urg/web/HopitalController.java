package com.urg.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.urg.Entites.Hopital;
import com.urg.dao.HopitalRepository;
import com.urg.services.HopitalService;

@RestController
@CrossOrigin("*")
public class HopitalController {
	
	@Autowired
	private HopitalService hopitalService;
	@Autowired
	private HopitalRepository hopitalRepository;
	//update
	@PutMapping("/hopitals/{id}")
	 public void updateHopital(@PathVariable("id") Integer id , @RequestBody Hopital hopital) {
	 hopitalService.modifierHopital(id, hopital);
	}
	//delete
	@DeleteMapping("/hopitals/{id}")
	public ResponseEntity<String> deleteClient(@PathVariable("id") int id) {
		try
		{
		   System.out.println("Delete hopital with ID = " + id + "...");
		   if (hopitalService.supprimerHopital(id)) {
			   return new ResponseEntity<>("hopital est bien  supprime!", HttpStatus.OK);

		     }
		   else {
			   return new ResponseEntity<>("Echec  suppression hopital!", HttpStatus.FAILED_DEPENDENCY);

		}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("", HttpStatus.EXPECTATION_FAILED);
		}
	}
	//ajout d'hopital
	@PostMapping(value = "/hopitals/create")
	public Boolean ajoutHopital(@RequestBody Hopital hopital) {
		try
		{
			System.out.println("creation  Hopital...");
			
			
			String  idhopital=""
					+hopital.getCapacite()
			        +((int) (Math.random()*(9-0)));
            Integer id=java.lang.Integer.valueOf(idhopital);
            hopital.setIdHopital(id);
            Hopital newHopital= new  Hopital();
            newHopital.setIdHopital(id);
            newHopital.setNomHopital(hopital.getNomHopital());
            newHopital.setCapacite(hopital.getCapacite());
            newHopital.setServices(null);
            newHopital.setUtilisateurs(null);
		    hopitalService.ajouterHopital(newHopital);
		    return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	@GetMapping("/hopitals/nomhopital/{recherche}")
	public Hopital getnomHopital(@PathVariable("recherche") String recherche)
	{
		try
		{
			System.out.println("recherche   Hopital par nom...");
			Hopital hopital = hopitalRepository.findByNomHopital(recherche);
			if (hopital!=null) {
				   hopital.setServices(null);
				   hopital.setUtilisateurs(null);
				return hopital;
			}

			return  null;
			
		}catch(Exception e)
		{
			e.printStackTrace();
			return  null;
		}
	}
			
	//recherche
	@GetMapping("/hopitals/searche/{recherche}")
	public List<Hopital> rechercheHopital(@PathVariable("recherche") String recherche)
	{
		try
		{
			
			 List<Hopital> listeHopital= hopitalRepository.findByNomHopitalContains(recherche);
			 List<Hopital> listeHopitalFinal=new ArrayList<>();
		     Iterator<Hopital> ithopital=listeHopital.iterator();

		     while(ithopital.hasNext())
			   {
				   Hopital hopitale=new Hopital();
				   Hopital emp=ithopital.next();
				   hopitale.setIdHopital(emp.getIdHopital());
				   hopitale.setNomHopital(emp.getNomHopital());
				   hopitale.setCapacite(emp.getCapacite());
				   hopitale.setServices(null);
				   hopitale.setUtilisateurs(null);
				   
				   listeHopitalFinal.add(hopitale);
			   }
			
			
			return listeHopitalFinal;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	

}
