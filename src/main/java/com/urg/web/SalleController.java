package com.urg.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.urg.Entites.Hopital;
import com.urg.Entites.Salle;
import com.urg.Entites.Service;
import com.urg.Entites.Utilisateur;
import com.urg.dao.SalleRepository;
import com.urg.dao.ServiceRepository;
import com.urg.services.SalleService;
import com.urg.services.ServiceService;

@RestController
@CrossOrigin("*")
public class SalleController {
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private ServiceRepository serviceRepository;
	@SuppressWarnings("unused")
	@Autowired
	private ServiceService serviceService;
	@Autowired
	private SalleRepository salleRepository;
	@Autowired
	private SalleService  salleService;
	
	//delete
		@DeleteMapping("/salles/{idSalle}")
		public ResponseEntity<String> deleteService(@PathVariable("idSalle") Integer idSalle) {
			try
			{
			   System.out.println("Delete hopital with ID = " +idSalle+ "...");
			   if (salleService.supprimerSallle(idSalle)) {
				   return new ResponseEntity<>("Salle est bien  supprime!", HttpStatus.OK);

			}else {
				return new ResponseEntity<>("Echec suppression salle", HttpStatus.EXPECTATION_FAILED);

			}
			}
			catch(Exception e)
			{
				return new ResponseEntity<>("Echec suppression salle", HttpStatus.EXPECTATION_FAILED);
			}
		}
		// grt salle par nom
		@GetMapping("/salles/masalle/{salle}")
		public Salle getmaSalle(@PathVariable("salle") String salle) {
			return salleService.findByNomSalle(salle);
			
		}
		//ajout salle
		@PostMapping("/salles/create")
	    public Boolean createService(@RequestBody Salle salle) {
	     try {  
				System.out.println("creation  Salle...");			
				String  idser=""+((int) (Math.random()*(9-0)))
				+((int) (Math.random()*(9-0)));
	            Integer id3=java.lang.Integer.valueOf(idser);  
	            salle.setIdSalle(id3);
	            //String num =salle.getService().getHopital().getNomHopital()+""+salle.getService().getNomService()+""+idser;
	            if (salleService.ajouterSalle(salle)) {
					return true;
				}
				return false;
	    
				}
	           
	            catch(Exception e)
	    		{
	    			e.printStackTrace();
	    			return false;
	    		}
	    }
		
		//update
		@PutMapping("/salles/{salleId}")
			    public ResponseEntity<?> updateUtilisateur(@PathVariable (value = "salleId") Integer salleId,
			                                 @RequestBody Salle salle) {
				try {
					   System.out.println("modification salle ");
					
					   Salle sall2= salleRepository.findByIdSalle(salleId);

			        if(sall2==null) {
			            throw new ResourceNotFoundException("Salle " + salleId+" non trouve");
			        }
			        else {
						Service service = serviceRepository.findByIdService(salle.getService().getIdService());
						if (service!=null) {
							sall2.setTypeSalle(salle.getTypeSalle());
							sall2.setLocalisation(salle.getLocalisation());
							sall2.setNomSalle(salle.getNomSalle());
							sall2.setService(service);
							if (salleService.modifierSalle(sall2)!=null) {
						        return ResponseEntity.ok().build();
								
							}
					        return null;

						}
						return null;
	
					}
						
		      	} catch(Exception e){
		         e.printStackTrace();
		         return null;
	             }
		}
	
		//recherhe par mot cle
		@GetMapping("/salles/{mtc}")
		public List<Salle> getSalleRecherche(@PathVariable("mtc") String mtc) {
			try
			{
			   System.out.println(" Recherche Salle Detail");
			   List<Salle> listeSalles = new ArrayList<Salle>();
			   List<Salle> listeSel=new ArrayList<Salle>();
			   salleRepository.findByNomSalleOrLocalisationOrTypeSalle(mtc, mtc, mtc).forEach(listeSalles::add);
			   Iterator<Salle> it=listeSalles.iterator();
			    while(it.hasNext())
			   {
			      Salle saleure=new Salle();
			      Salle emputSalle=it.next();
			      Service service= serviceRepository.findByIdService(emputSalle.getService().getIdService());
			      if (service!=null) {
			    	  
			    	    saleure.setIdSalle(emputSalle.getIdSalle());
			    	    saleure.setLocalisation(emputSalle.getLocalisation());
			    	    saleure.setNomSalle(emputSalle.getNomSalle());
			    	    saleure.setTypeSalle(emputSalle.getTypeSalle());			  
					    Service pays=emputSalle.getService();
					    if(pays !=null) {
					   Service pys=new Service(); 
					   pys.setIdService(pays.getIdService());
					   pys.setNomService(pays.getNomService());
					   pys.setSalles(null);
					   saleure.setService(pys);
					 
					   listeSel.add(saleure);
					   
				   }
			      
			    	  
			      }
					
				}
			      
			    	
			   return listeSel;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}

}
