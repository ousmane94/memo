package com.urg.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.urg.Entites.Specialite;
import com.urg.dao.SpecialiteRepository;
import com.urg.services.SpeciaiteService;

@RestController
@CrossOrigin("*")
public class SpecialiteController {
	
	@Autowired
	private SpecialiteRepository specialiteRepository;
	@Autowired
	private SpeciaiteService speciaiteService;

	
	
	//delete
		@DeleteMapping("/specialites/{idSpecialite}")
		public ResponseEntity<String> deleteService(@PathVariable("idSpecialite") Integer idSpecialite) {

			try
			{
			   System.out.println("Delete Service with ID = " + idSpecialite + "...");
			   if (speciaiteService.supprimerSpecialite(idSpecialite)) {
				   return new ResponseEntity<>("Service est bien  supprime!", HttpStatus.OK);

			     }
			   else {
				   return new ResponseEntity<>("Echec  suppression Service!", HttpStatus.FAILED_DEPENDENCY);
			}
			}
			catch(Exception e)
			{
				return new ResponseEntity<>("", HttpStatus.EXPECTATION_FAILED);
			}
		}
		//ajouter specialite
		@PostMapping("/specialites/create")
	    public Boolean createSpecialite(@RequestBody Specialite specialite) {
	     try {  
				System.out.println("creation  Specialite...");			
				String  idser=""+((int) (Math.random()*(9-0)))
				+((int) (Math.random()*(9-0)));
	            Integer id3=java.lang.Integer.valueOf(idser);  
	            specialite.setIdSpecialite(id3);
	            if (speciaiteService.ajouterSpecialite(specialite)) {
					return true;
				}
				return false;
				}
	            catch(Exception e)
	    		{
	    			e.printStackTrace();
	    			return false;
	    		}
	    }
		// get Speciallite par nom
		@GetMapping("/specialites/nomSpecialite/{specialite}")
		public Specialite getnomSpecialite(@PathVariable("specialite") String specialite) {
			return speciaiteService.findByNomSpecialite(specialite);		
		}
		//update
		@PutMapping("/Specialites/{specialiteId}")
			    public ResponseEntity<?> updateUtilisateur(@PathVariable (value = "specialiteId") Integer specialiteId,
			                                 @RequestBody Specialite specialite) {
				try {
					System.out.println("modification specialite ");
			        Specialite sp2= specialiteRepository.findByIdSpecialite(specialiteId);
			        if(sp2==null) {
			            throw new ResourceNotFoundException("Specialite " + specialiteId+" not found");
			        }
			        else {
							sp2.setNomSpecialite(specialite.getNomSpecialite());
							if (speciaiteService.modifierSpecialite(sp2)!=null) {
						        return ResponseEntity.ok().build();	
						}
						return null;
					}
						
		      	} catch(Exception e){
		         e.printStackTrace();
		         return null;
	          }
		}
			       

}
