package com.urg.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.urg.Entites.Hopital;
import com.urg.Entites.Medecin;
import com.urg.Entites.Utilisateur;
import com.urg.dao.HopitalRepository;
import com.urg.dao.MedecinRepository;
import com.urg.services.MedecinService;

public class MedecinController {
	@Autowired
	private MedecinRepository medecinRepository;
	@Autowired
	private MedecinService medecinService;
	@Autowired
	private HopitalRepository hopitalRepository;
	
	
	
	
	//ajout Utilisateur
		@PostMapping("/utilisateurs/create")
		public Boolean ajoutHopital(@RequestBody Medecin medecin) {
			try
			{
				System.out.println("creation  medecin...");
				  String iduser1=""+((int) (Math.random()*(9-0))) +((int)
				  (Math.random()*(9-0))) +((int) (Math.random()*(9-0))); Integer
				  uss1=java.lang.Integer.valueOf(iduser1); 
				  Medecin newmedecin= new Medecin(); 
				  newmedecin.setIdPersonne(uss1);
				  newmedecin.setAdresse(medecin.getAdresse());
				  newmedecin.setEmail(medecin.getEmail());
				  newmedecin.setPassword(medecin.getPassword());
				  newmedecin.setMatricule(medecin.getMatricule());
				  newmedecin.setSpecialite(medecin.getSpecialite());
				  newmedecin.setGenre(medecin.getGenre());
				  newmedecin.setNom(medecin.getNom());
				  newmedecin.setPrenom(medecin.getPrenom());
				  newmedecin.setProfils("Medecin");
				  newmedecin.setTelephone(medecin.getTelephone());
				  newmedecin.setUsername(medecin.getUsername());
				  Hopital hopital = hopitalRepository.findByIdHopital(medecin.getHopital().getIdHopital());
				  newmedecin.setHopital(hopital); 
				  Medecin newUtil= medecinService.ajouterMedecin(newmedecin);
		         if (newUtil!=null) {
				  return true;
			   }
		         return false;
		    
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return false;
			}
		}
		
		//bon ajout utilisateur
		 @PostMapping("/hopitals/{hopitalId}/medecins")
		    public Boolean createUtilisateur(@PathVariable (value = "hopitalId") Integer hopitalId,
		                                  @RequestBody Medecin medecin) {
		     try {   Hopital hopital = hopitalRepository.findByIdHopital(hopitalId);
		            medecin.setHopital(hopital);
		            Medecin userUtilisateur = medecinService.ajouterMedecin(medecin);
		            if (userUtilisateur==null) {
		                System.out.println("Identification "+hopitalId+" non trouvé ");
		                return null;
					}
		            else {
		            	userUtilisateur.setHopital(null);
						return true;
					}
		     }
		            catch(Exception e)
		    		{
		    			e.printStackTrace();
		    			return false;
		    		}
		    }


}
