package com.urg.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.urg.Entites.Hopital;
import com.urg.Entites.Medecin;
import com.urg.Entites.Personne;
import com.urg.Entites.Utilisateur;
import com.urg.dao.HopitalRepository;
import com.urg.dao.PersonneRepository;
import com.urg.dao.UtilisateurRepository;
import com.urg.services.CompteServiceImpl;

@RestController
@RequestMapping("/user")
public class UtilisateurController   {
	
	@Autowired
	private CompteServiceImpl accountService;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private HopitalRepository hopitalRepository;
	@Autowired
	private PersonneRepository personneRepository;

	//ajout Utilisateur
	@PostMapping("/utilisateurs/create")
	public Boolean ajoutHopital(@RequestBody Utilisateur utilisateur) {
		try
		{
			System.out.println("creation  utilisateur...");
			  String iduser1=""+((int) (Math.random()*(9-0))) +((int)
			  (Math.random()*(9-0))) +((int) (Math.random()*(9-0))); Integer
			  uss1=java.lang.Integer.valueOf(iduser1); 
			  Utilisateur newUtilisateur= new Utilisateur(); 
			  newUtilisateur.setIdPersonne(uss1);
			  newUtilisateur.setAdresse(utilisateur.getAdresse());
			  newUtilisateur.setEmail(utilisateur.getEmail());
			  newUtilisateur.setPassword(utilisateur.getPassword());
			  newUtilisateur.setGenre(utilisateur.getGenre());
			  newUtilisateur.setNom(utilisateur.getNom());
			  newUtilisateur.setPrenom(utilisateur.getPrenom());
			  newUtilisateur.setProfils(utilisateur.getProfils());
			  newUtilisateur.setTelephone(utilisateur.getTelephone());
			  newUtilisateur.setUsername(utilisateur.getUsername());
			  Hopital hopital = hopitalRepository.findByIdHopital(utilisateur.getHopital().getIdHopital());
			  newUtilisateur.setHopital(hopital); 
			  Utilisateur newUtil= accountService.ajouterUtilisateur(newUtilisateur);
	         if (newUtil!=null) {
			  return true;
		   }
	         return false;
	    
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	//bon ajout utilisateur
	 @PostMapping("/hopitals/{hopitalId}/utilisateurs")
	    public Boolean createUtilisateur(@PathVariable (value = "hopitalId") Integer hopitalId,
	                                  @RequestBody Utilisateur utilisateur) {
	     try {   Hopital hopital = hopitalRepository.findByIdHopital(hopitalId);
	            utilisateur.setHopital(hopital);
	            Utilisateur userUtilisateur = accountService.ajouterUtilisateur(utilisateur);
	            if (userUtilisateur==null) {
	                System.out.println("Identification "+hopitalId+" non trouvé ");
	                return null;
				}
	            else {
	            	userUtilisateur.setHopital(null);
					return true;
				}
	     }
	            catch(Exception e)
	    		{
	    			e.printStackTrace();
	    			return false;
	    		}
	    }

	
	//update
	@PutMapping("/utilisateur/{id}")
	public void updateProduit(@PathVariable("id") Integer id , @RequestBody Utilisateur utilisateur) {
			accountService.updeateUtilisateur(id, utilisateur);
		}
	
	
	//get Personné logué
	@GetMapping("/usernameUtilisateur/{username}")
	public Personne getProduit(@PathVariable("username") String username) {
		try
		{
		   System.out.println("Get username Utilisateur...");
		   
		   
		    Utilisateur personne= accountService.lectureByUsername(username);
		   if(personne==null) 
		   {
			   System.out.println("n'est pas utilisateur");
			  
			    Medecin med = accountService.connectByUsername(username);
			    if (med==null) {
			    	
			    	return null;
					
				}else {
					Medecin utilisateure = new  Medecin();
					utilisateure.setAdresse(med.getAdresse());
			    	utilisateure.setEmail(med.getEmail());
			    	utilisateure.setGenre(med.getGenre());
			    	utilisateure.setIdPersonne(med.getIdPersonne());
			    	utilisateure.setNom(med.getNom());
			    	utilisateure.setPrenom(med.getPrenom());
			    	utilisateure.setPassword(med.getPassword());
			    	utilisateure.setProfils(med.getProfils());
			    	utilisateure.setTelephone(med.getTelephone());
			    	utilisateure.setUsername(med.getUsername());
			    	
				    Hopital pays=med.getHopital();
				   if(pays !=null) {
				   Hopital pys=new Hopital(); 
				   pys.setIdHopital(pays.getIdHopital());
				   pys.setNomHopital(pays.getNomHopital());
				   pys.setCapacite(pays.getCapacite());
				   pys.setUtilisateurs(null);
				   pys.setServices(null);
				   utilisateure.setHopital(pys);
				   }
				   return utilisateure;

					
				}

		   }
		   System.out.println(" est utilisateur");
		    Utilisateur utilisateure = new  Utilisateur();
			utilisateure.setAdresse(personne.getAdresse());
			utilisateure.setEmail(personne.getEmail());
	    	utilisateure.setGenre(personne.getGenre());
	    	utilisateure.setIdPersonne(personne.getIdPersonne());
	    	utilisateure.setNom(personne.getNom());
	    	utilisateure.setPrenom(personne.getPrenom());	
	    	utilisateure.setPassword(personne.getPassword());
	    	utilisateure.setProfils(personne.getProfils());
	    	utilisateure.setTelephone(personne.getTelephone());
	    	utilisateure.setUsername(personne.getUsername());
		    Hopital pays=personne.getHopital();
		   if(pays !=null) {
		   Hopital pys=new Hopital(); 
		   pys.setIdHopital(pays.getIdHopital());
		   pys.setNomHopital(pays.getNomHopital());
		   pys.setCapacite(pays.getCapacite());
		   pys.setUtilisateurs(null);
		   pys.setServices(null);
		   utilisateure.setHopital(pys);
		   }
		   
		   return utilisateure;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	@GetMapping("/utilisateur")
	public List<Personne> getUtilisateurs(){
		try
		{
		   System.out.println("Get all Utilisateur...");
		    List<Personne> listeUtilisateurs= (List<Personne>) personneRepository.findAll();
		   if(listeUtilisateurs==null) 
		   {
			   return null;
		   }
			
			return listeUtilisateurs;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	
	//Get All utilisateur avec plus de Details bon
		@GetMapping("/utilisateurs")
		public List<Personne> getAllUtilisateursList() {
			try
			{
			   System.out.println("Get All Utilisateur Detail");
			   List<Personne> listeUtilisateur = new ArrayList<Personne>();
			   List<Personne> listeEmp=new ArrayList<Personne>();
			  //avant  utilisateurRepository.findAll().forEach(listeUtilisateur::add);
			   utilisateurRepository.findlisteUtilisateurs().forEach(listeUtilisateur::add);

			   Iterator<Personne> it=listeUtilisateur.iterator();
			    while(it.hasNext())
			   {
			    	Utilisateur utilisateure=new Utilisateur();
				      Personne emputUtilisateur=it.next();
				      
				      Hopital hopital= hopitalRepository.findByIdHopital(emputUtilisateur.getHopital().getIdHopital());
				      if (hopital!=null) {
				    	  System.out.println("recherche de l hopital"+hopital.getNomHopital());
				    	    utilisateure.setAdresse(emputUtilisateur.getAdresse());
					    	utilisateure.setEmail(emputUtilisateur.getEmail());
					    	utilisateure.setGenre(emputUtilisateur.getGenre());
					    	utilisateure.setIdPersonne(emputUtilisateur.getIdPersonne());
					    	utilisateure.setNom(emputUtilisateur.getNom());
					    	utilisateure.setPrenom(emputUtilisateur.getPrenom());
					    	utilisateure.setPassword(emputUtilisateur.getPassword());
					    	utilisateure.setProfils(emputUtilisateur.getProfils());
					    	utilisateure.setTelephone(emputUtilisateur.getTelephone());
					    	utilisateure.setUsername(emputUtilisateur.getUsername());
						    Hopital pays=emputUtilisateur.getHopital();
						    if(pays !=null) {
						    Hopital pys=new Hopital(); 
						   pys.setIdHopital(pays.getIdHopital());
						   pys.setNomHopital(pays.getNomHopital());
						   pys.setCapacite(pays.getCapacite());
						   pys.setUtilisateurs(null);
						   pys.setServices(null);
						   utilisateure.setHopital(pys);
						   listeEmp.add(utilisateure);
						   
					   }
				      
				    	  
				      }
						
					}
				      
				    	
				   return listeEmp;
				}
				catch(Exception e)
				{
					e.printStackTrace();
					return null;
				}
			}
		//recherhe par mot cle
				@GetMapping("/utilisateurs/{mtc}")
				public List<Utilisateur> getAllUtilisateuRecherche(@PathVariable("mtc") String mtc) {
					try
					{
					   System.out.println("Get Recherche Utilisateur Detail");
					   List<Utilisateur> listeUtilisateur = new ArrayList<Utilisateur>();
					   List<Utilisateur> listeEmp=new ArrayList<Utilisateur>();
					   utilisateurRepository.findByAdresseOrNomOrPrenomOrProfilsOrPasswordOrUsername(mtc, mtc, mtc, mtc, mtc, mtc).forEach(listeUtilisateur::add);
					   Iterator<Utilisateur> it=listeUtilisateur.iterator();
					    while(it.hasNext())
					   {
					      Utilisateur utilisateure=new Utilisateur();
					      Utilisateur emputUtilisateur=it.next();
					      Hopital hopital= hopitalRepository.findByIdHopital(emputUtilisateur.getHopital().getIdHopital());
					      if (hopital!=null) {
					    	  
					    	    utilisateure.setAdresse(emputUtilisateur.getAdresse());
						    	utilisateure.setEmail(emputUtilisateur.getEmail());
						    	utilisateure.setGenre(emputUtilisateur.getGenre());
						    	utilisateure.setIdPersonne(emputUtilisateur.getIdPersonne());
						    	utilisateure.setNom(emputUtilisateur.getNom());
						    	utilisateure.setPrenom(emputUtilisateur.getPrenom());
						    	utilisateure.setPassword(emputUtilisateur.getPassword());
						    	utilisateure.setProfils(emputUtilisateur.getProfils());
						    	utilisateure.setTelephone(emputUtilisateur.getTelephone());
						    	utilisateure.setUsername(emputUtilisateur.getUsername());
							    Hopital pays=emputUtilisateur.getHopital();
							    if(pays !=null) {
							    Hopital pys=new Hopital(); 
							   pys.setIdHopital(pays.getIdHopital());
							   pys.setNomHopital(pays.getNomHopital());
							   pys.setCapacite(pays.getCapacite());
							   pys.setUtilisateurs(null);
							   pys.setServices(null);
							   utilisateure.setHopital(pys);
							   listeEmp.add(utilisateure);
							   
						   }
					      
					    	  
					      }
							
						}
					      
					    	
					   return listeEmp;
					}
					catch(Exception e)
					{
						e.printStackTrace();
						return null;
					}
				}
		// delete utilisateur
		@DeleteMapping("/hopitals/{hopitalId}/utilisateurs/{utilisateurId}")
	    public ResponseEntity<?> deleteUtilisateurs(@PathVariable (value = "utilisateurId") Integer utilisateurId) {
	        try { 
			accountService.deleteUtilisateur(utilisateurId);
	         return ResponseEntity.ok().build();
	            
	        }catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
	    }
	
		//delete
		@DeleteMapping("/utilisateurs/delete/{utilisateurId}")
		public ResponseEntity<String> deleteClient(@PathVariable("utilisateurId") Integer utilisateurId) {
			try
			{
			   System.out.println("Delete hopital with ID = "+utilisateurId+ "...");
			   accountService.deleteUtilisateur(utilisateurId);
			   return new ResponseEntity<>("utilisateur est bien  supprime!", HttpStatus.OK);
			}
			catch(Exception e)
			{
				return new ResponseEntity<>("", HttpStatus.EXPECTATION_FAILED);
			}
		}
	//update
		@PutMapping("/hopitals/{hopitalId}/utilisateurs/{utilisateurId}")
	    public ResponseEntity<?> updateUtilisateur(@PathVariable (value = "hopitalId") Integer hopitalId,
	                                 @PathVariable (value = "utilisateurId") Integer utilisateurId,
	                                 @RequestBody Utilisateur utilisateur) {
		try {
			
			Hopital hopital = hopitalRepository.findByIdHopital(hopitalId);
			
	        if(hopital==null) {
	            throw new ResourceNotFoundException("hopitalId " + hopitalId + " not found");
	        }
	        Utilisateur userUtilisateur= utilisateurRepository.findByIdPersonne(utilisateurId);
	        userUtilisateur.setAdresse(utilisateur.getAdresse());
	        userUtilisateur.setEmail(utilisateur.getEmail());
	        userUtilisateur.setGenre(utilisateur.getGenre());
	        userUtilisateur.setNom(utilisateur.getNom());
	        userUtilisateur.setPrenom(utilisateur.getPrenom());
	        userUtilisateur.setProfils(utilisateur.getProfils());
	        userUtilisateur.setTelephone(utilisateur.getTelephone());
	        userUtilisateur.setUsername(utilisateur.getUsername());
	        userUtilisateur.setHopital(hopital);
	        if (utilisateur.getPassword()!=null) {
	        	userUtilisateur.setPassword(utilisateur.getPassword());
				
			}
	       Utilisateur use=  accountService.updeateUtilisateur(userUtilisateur.getIdPersonne(), userUtilisateur);
	       if (use!=null) 
		         return ResponseEntity.ok().build();
	       return null;
	        
	       }catch(Exception e){
	         e.printStackTrace();
	         return null;
             }
		}
		
		   

}





 
