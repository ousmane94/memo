package com.urg.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.urg.Entites.Hopital;
import com.urg.Entites.Service;
import com.urg.dao.HopitalRepository;
import com.urg.dao.ServiceRepository;
import com.urg.services.ServiceService;


@RestController
@CrossOrigin("*")
public class ServiceController {
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private ServiceRepository serviceRepository;
	@Autowired
	private ServiceService serviceService;
	@Autowired
	private HopitalRepository hopitalRepository;
	
	
	
	//delete
	@DeleteMapping("/services/{idService}")
	public ResponseEntity<String> deleteService(@PathVariable("idService") Integer idService) {
		/*try
		{
		   System.out.println("Delete hopital with ID = " +idService+ "...");
		   serviceService.serviceService(idService);
		   return new ResponseEntity<>("Service est bien  supprime!", HttpStatus.OK);
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("", HttpStatus.EXPECTATION_FAILED);
		}*/
		try
		{
		   System.out.println("Delete Service with ID = " + idService + "...");
		   if (serviceService.supprimerService(idService)) {
			   return new ResponseEntity<>("Service est bien  supprime!", HttpStatus.OK);

		     }
		   else {
			   return new ResponseEntity<>("Echec  suppression Service!", HttpStatus.FAILED_DEPENDENCY);

		}
		}
		catch(Exception e)
		{
			return new ResponseEntity<>("", HttpStatus.EXPECTATION_FAILED);
		}
	
	}
	//ajout d'service
	//@PostMapping("/hopitals/{hopitalId}/services")
	@PostMapping("/services/create")
    public Boolean createService(@RequestBody Service service) {
     try {  
			System.out.println("creation  Service...");			
			String  idser=""+((int) (Math.random()*(9-0)))
			+((int) (Math.random()*(9-0)));
            Integer id3=java.lang.Integer.valueOf(idser);  
            service.setIdService(id3);
            service.setNomService(service.getNomService()+"-"+service.getHopital().getNomHopital());
            if (serviceService.ajouterService(service)) {
				return true;
			}
			return false;
    
			}
           
            catch(Exception e)
    		{
    			e.printStackTrace();
    			return false;
    		}
    }
	// get service par nom
	@GetMapping("/services/nomService/{service}")
	public Service getnomservice(@PathVariable("service") String service) {
		return serviceService.findServiceByNom(service);	
		
	}
		
 //Get All Service avec plus de Details
	@GetMapping("/services/mesServices/{hopital}")
		public List<Service> getAllServicesList(@PathVariable("hopital") String hopital) {
		
		try
		{
		         Hopital monHopital = hopitalRepository.findByNomHopital(hopital);
		         if (monHopital==null) {
		        	 return null;
					
				}else {
					
			
				   System.out.println("Get All Service par hopital");
					  // List<Service> listeService = new ArrayList<Service>();
					   List<Service> listeEmp=new ArrayList<Service>();
					   @SuppressWarnings("unchecked")
					 List<Service> listeService = serviceRepository.findByHopital(monHopital);
					   Iterator<Service> it=listeService.iterator();
					    while(it.hasNext())
					   {
					    	Service service=new Service();
					    	Service empuService=it.next();
					    	service.setIdService(empuService.getIdService());
					    	service.setNomService(empuService.getNomService());
					    	service.setMedecins(null);
					    	service.setSalles(null);
						    Hopital pays=empuService.getHopital();
						    Hopital pys=new Hopital(); 
						   if(pays !=null) {
						   pys.setIdHopital(pays.getIdHopital());
						   pys.setNomHopital(pays.getNomHopital());
						   pys.setCapacite(pays.getCapacite());
						   pys.setUtilisateurs(null);
						   pys.setServices(null);
						   }
						   service.setHopital(pys);
						   listeEmp.add(service);
					   }
					   return listeEmp;
					}
		}
					catch(Exception e)
					{
						e.printStackTrace();
						return null;
					}
				}
		
		//update
		@PutMapping("/services/{serviceId}")
			    public ResponseEntity<?> updateUtilisateur(@PathVariable (value = "serviceId") Integer serviceId,
			                                 @RequestBody Service service) {
				try {
					   System.out.println("modification Service ");
					
			        Service ser2= serviceRepository.findByIdService(serviceId);

			        if(ser2==null) {
			            throw new ResourceNotFoundException("service " + serviceId+" not found");
			        }
			        else {
						Hopital hopital = hopitalRepository.findByIdHopital(service.getHopital().getIdHopital());
						if (hopital!=null) {
							ser2.setNomService(service.getNomService());
							ser2.setHopital(hopital);
							if (serviceService.modifierService(ser2)!=null) {
						        return ResponseEntity.ok().build();
								
							}
					        return null;

						}
						return null;
	
					}
						
		      	} catch(Exception e){
		         e.printStackTrace();
		         return null;
	             }
		}
			       
	}


