package com.urg.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.urg.Entites.Salle;
import com.urg.Entites.Service;
import com.urg.Entites.Lit;
import com.urg.dao.LitRepository;
import com.urg.dao.SalleRepository;
import com.urg.services.Litservice;
import com.urg.services.SalleService;

@RestController
@CrossOrigin("*")
public class LitController {
	
	@Autowired
	private LitRepository litRepository;
	@Autowired
	private Litservice litservice;
	@Autowired
	private SalleRepository salleRepository;
	@Autowired
	private SalleService  salleService;
	
	//delete
			@DeleteMapping("/lits/{idlit}")
			public ResponseEntity<String> deleteService(@PathVariable("idlit") Integer idlit) {
				try
				{
				   System.out.println("Delete lit with ID = " +idlit+ "...");
				   if (litservice.supprimerLit(idlit)) {
					   return new ResponseEntity<>("lit est bien  supprime!", HttpStatus.OK);

				}else {
					return new ResponseEntity<>("Echec suppression Lit", HttpStatus.EXPECTATION_FAILED);

				}
				}
				catch(Exception e)
				{
					return new ResponseEntity<>("Echec suppression lit", HttpStatus.EXPECTATION_FAILED);
				}
			}
			
			//ajout salle
			@PostMapping("/lits/create")
		    public Boolean createService(@RequestBody Lit lit) {
		     try {  
					System.out.println("creation  Lit...");			
					String  idser=""+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)));
		            Integer id3=java.lang.Integer.valueOf(idser);  
		            lit.setIdLit(id3);
		            //String num =salle.getService().getHopital().getNomHopital()+""+salle.getService().getNomService()+""+idser;
		            if (litservice.ajouterLit(lit)) {
						return true;
					}
					return false;
		    
					}
		           
		            catch(Exception e)
		    		{
		    			e.printStackTrace();
		    			return false;
		    		}
		    }
			
			
			//update
			@PutMapping("/lits/{litId}")
				    public ResponseEntity<?> updateUtilisateur(@PathVariable (value = "litId") Integer litId,
				                                 @RequestBody Lit lit) {
					try {
						   System.out.println("modification lit ");
						
						   Lit lit2= litRepository.findByIdLit(litId);

				        if(lit2==null) {
				            throw new ResourceNotFoundException("Lit " + litId+" non trouve");
				        }
				        else {
							Salle salle = salleService.findByIdSalle(lit.getSalle().getIdSalle());
							if (salle!=null) {
								lit2.setEtat(lit.getEtat());
								lit2.setNumeroLit(lit.getNumeroLit());
								lit2.setType_de_lit(lit.getType_de_lit());
								lit2.setSalle(salle);								
								if (litservice.modifierLit(lit2)!=null) {
							        return ResponseEntity.ok().build();
									
								}
						        return null;

							}
							return null;
		
						}
							
			      	} catch(Exception e){
			         e.printStackTrace();
			         return null;
		             }
			}
		

}
