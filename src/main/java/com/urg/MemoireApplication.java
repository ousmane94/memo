package com.urg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.urg.Entites.Hopital;
import com.urg.Entites.Lit;
import com.urg.Entites.Salle;
import com.urg.Entites.Service;
import com.urg.Entites.Specialite;
import com.urg.Entites.Utilisateur;

@SpringBootApplication
public class MemoireApplication implements CommandLineRunner {
	@Autowired
	private RepositoryRestConfiguration restConfiguration;
	
	@Bean
	public BCryptPasswordEncoder getBCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public static void main(String[] args) {
		SpringApplication.run(MemoireApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// affiche de l'id dans json
		 restConfiguration.exposeIdsFor(
				 Hopital.class,Utilisateur.class,
				 Service.class,Salle.class,
				 Lit.class, Specialite.class);
	
		
	}

}
