package com.urg.Entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//@Table(name = "medecin", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
@DiscriminatorValue("med")
public  class Medecin extends Personne implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String matricule ;
	//Service
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idService", nullable = true)
	private Service service;
	//Consultation
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "medecin")
	private List<Consultation> consultations;
	//specialite
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idSpecialite", nullable = true)
	private Specialite specialite;
	
	
	public Medecin(int idPersonne, String prenom, String nom, String email, int telephone, String genre, String adresse,
			String username, String password, String profils, Hopital hopital, String matricule, Service service,
			List<Consultation> consultations, Specialite specialite) {
		super(idPersonne, prenom, nom, email, telephone, genre, adresse, username, password, profils, hopital);
		this.matricule = matricule;
		this.service = service;
		this.consultations = consultations;
		this.specialite = specialite;
	}

	public Medecin() {
	}
	
	public String getMatricule() {
		return matricule;
	}


	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public Service getService() {
		return service;
	}


	public void setService(Service service) {
		this.service = service;
	}


	public List<Consultation> getConsultations() {
		return consultations;
	}


	public void setConsultations(List<Consultation> consultations) {
		this.consultations = consultations;
	}


	public Specialite getSpecialite() {
		return specialite;
	}


	public void setSpecialite(Specialite specialite) {
		this.specialite = specialite;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Medecin other = (Medecin) obj;

		if (matricule != other.matricule)
			return false;
		return true;
	}
	
}
