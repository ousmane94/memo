package com.urg.Entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "consultation", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Consultation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private int idConsultation;
	private  Date dateConsultation ;
	private String synthese;
	private String typeConsultation;
	//hospitalisation
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "consultation")
	private List<Hospitalisation> hospitalisations;
	//Medecin
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPersonne", nullable = false)
	private Medecin medecin;
	//Patient
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPatient", nullable = false)
	private Patient patient;
	//traitement
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "consultation")
	private List<Traitement> traitements;
	//constante
	//@ManyToMany
	//@JoinTable(name = "appel_constante", joinColumns = @JoinColumn(name = "idConsultation"), 
	//inverseJoinColumns = @JoinColumn(name = "nom"))
	//private List<Constante> constantes;
   // @OneToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL,mappedBy = "consultation")
    //private Hospitalisation hospitalisation;
	
	

}
