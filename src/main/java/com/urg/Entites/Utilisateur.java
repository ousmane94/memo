package com.urg.Entites;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
@DiscriminatorValue("Utl")
public class Utilisateur  extends Personne{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Utilisateur(int idPersonne, String prenom, String nom, String email, int telephone, String genre,
			String adresse, String username, String password, String profils, Hopital hopital) {
		super(idPersonne, prenom, nom, email, telephone, genre, adresse, username, password, profils, hopital);
		// TODO Auto-generated constructor stub
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	


}
