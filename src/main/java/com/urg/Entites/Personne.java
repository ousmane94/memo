
package com.urg.Entites;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "personne", catalog = "urgenceDB"  )
@Data @NoArgsConstructor @AllArgsConstructor @ToString
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE) 
@DiscriminatorColumn(name="TYPE_PER",discriminatorType=DiscriminatorType.STRING,length=3)
public abstract class Personne implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	private int idPersonne;
	private String prenom;
	private String nom;
	private String email;
	private int telephone;
	private String genre;
	private String adresse;
	@Column(unique =true )
	private String username;
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;
	private String profils;
	@ManyToOne(fetch = FetchType.LAZY , optional = false )
	@JoinColumn(name = "idHopital" , nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    //@JsonIgnore
	private Hopital hopital;
	/*
	 * public Utilisateur(int idUtilisateur, String prenom, String nom, String
	 * email, int telephone, String genre, String adresse, String username, String
	 * password, String profils, Hopital hopital) { super(); this.idUtilisateur =
	 * idUtilisateur; this.prenom = prenom; this.nom = nom; this.email = email;
	 * this.telephone = telephone; this.genre = genre; this.adresse = adresse;
	 * this.username = username; this.password = password; this.profils = profils;
	 * this.hopital = hopital; }
	 */
	
	@JsonCreator
	public Personne(
			@JsonProperty("idPersonne")int idPersonne, 
			@JsonProperty("prenom") String prenom,
			@JsonProperty("nom") String nom, 
			@JsonProperty("email") String email,
			@JsonProperty("telephone") int telephone,
			@JsonProperty("genre") String genre,
			@JsonProperty("adresse")String adresse,
			@JsonProperty("username")String username,
			@JsonProperty("password") String password,
			@JsonProperty("profils")String profils,
			@JsonProperty("hopital")Hopital hopital) {
		this.idPersonne = idPersonne;
		this.prenom = prenom;
		this.nom = nom;
		this.email = email;
		this.telephone = telephone;
		this.genre = genre;
		this.adresse = adresse;
		this.username = username;
		this.password = password;
		this.profils = profils;
		this.hopital = hopital;
	}
	


	public Personne() {
	}


	public int getIdPersonne() {
		return idPersonne;
	}


	public void setIdPersonne(int idPersonne) {
		this.idPersonne = idPersonne;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getTelephone() {
		return telephone;
	}


	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}


	public String getGenre() {
		return genre;
	}


	public void setGenre(String genre) {
		this.genre = genre;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getProfils() {
		return profils;
	}


	public void setProfils(String profils) {
		this.profils = profils;
	}


	public Hopital getHopital() {
		return hopital;
	}


	public void setHopital(Hopital hopital) {
		this.hopital = hopital;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((genre == null) ? 0 : genre.hashCode());
		result = prime * result + ((hopital == null) ? 0 : hopital.hashCode());
		result = prime * result + idPersonne;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + ((profils == null) ? 0 : profils.hashCode());
		result = prime * result + telephone;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personne other = (Personne) obj;
		if (adresse == null) {
			if (other.adresse != null)
				return false;
		} else if (!adresse.equals(other.adresse))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (genre == null) {
			if (other.genre != null)
				return false;
		} else if (!genre.equals(other.genre))
			return false;
		if (hopital == null) {
			if (other.hopital != null)
				return false;
		} else if (!hopital.equals(other.hopital))
			return false;
		if (idPersonne != other.idPersonne)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (profils == null) {
			if (other.profils != null)
				return false;
		} else if (!profils.equals(other.profils))
			return false;
		if (telephone != other.telephone)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Personne [idPersonne=" + idPersonne + ", prenom=" + prenom + ", nom=" + nom + ", email="
				+ email + ", telephone=" + telephone + ", genre=" + genre + ", adresse=" + adresse + ", username="
				+ username + ", password=" + password + ", profils=" + profils + ", hopital=" + hopital + "]";
	}


	

}
