package com.urg.Entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "hospitalisation", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Hospitalisation implements Serializable{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	 private int  idHospitalisation ;
     private Date dateAdmmission;
     private String typeAdmission;
     private String motifAdmission;
     private String prenom_Accompagnant;
     private String nom_Accompagnant;
     private String lien_Parente;
     private Date dateEntree;
     private Date dateSortie;
     private String motifSortie;
     private String resultattSortie;
     private Date dateDeceeDate;
     private String motifDecee;
     //lit
 	@ManyToOne(fetch = FetchType.LAZY)
 	@JoinColumn(name = "idLit", nullable = false)
 	private Lit lit;
    //Consultation
 	@ManyToOne(fetch = FetchType.LAZY)
 	@JoinColumn(name = "idConsultation", nullable = false)
 	private Consultation consultation;

     //@ManyToOne(fetch = FetchType.LAZY)
 	 //@JoinColumn(name = "idLit", nullable = false)
 	 //private Lit lit;
     //consultation
     //@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
     //private Consultation consultation;
     

     
     
     
     
     
     
	

}
