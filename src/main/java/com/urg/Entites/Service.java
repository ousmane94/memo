package com.urg.Entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "service", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Service implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private int idService;
	private String nomService;
	//private int nombre_de_lit;
	//hpital
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHopital")
	private Hopital hopital;
	//Salle
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "service")
	private List<Salle> salles;
	//medecin
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "service")
	private List<Medecin> medecins;
	public Service() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Service(int idService, String nomService/*, int nombre_de_lit*/, Hopital hopital) {
		super();
		this.idService = idService;
		this.nomService = nomService;
		//this.nombre_de_lit = nombre_de_lit;
		this.hopital = hopital;
	}
	public int getIdService() {
		return idService;
	}
	public void setIdService(int idService) {
		this.idService = idService;
	}
	public String getNomService() {
		return nomService;
	}
	public void setNomService(String nomService) {
		this.nomService = nomService;
	}
	/*public int getNombre_de_lit() {
		return nombre_de_lit;
	}
	public void setNombre_de_lit(int nombre_de_lit) {
		this.nombre_de_lit = nombre_de_lit;
	}*/
	public Hopital getHopital() {
		return hopital;
	}
	public void setHopital(Hopital hopital) {
		this.hopital = hopital;
	}
	public List<Salle> getSalles() {
		return salles;
	}
	public void setSalles(List<Salle> salles) {
		this.salles = salles;
	}
	public List<Medecin> getMedecins() {
		return medecins;
	}
	public void setMedecins(List<Medecin> medecins) {
		this.medecins = medecins;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	 
	

}
