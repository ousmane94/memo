package com.urg.Entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "lit", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Lit implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private int idLit;
	private String numeroLit;
	private String etat;
	private String type_de_lit;
	//salle
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idSalle", nullable = false)
	private Salle salle;
	//hospitalisation
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "lit")
	private List<Hospitalisation> hospitalisations;
	
	
	public Lit() {
		// TODO Auto-generated constructor stub
	}


	public int getIdLit() {
		return idLit;
	}


	public void setIdLit(int idLit) {
		this.idLit = idLit;
	}


	public String getNumeroLit() {
		return numeroLit;
	}


	public void setNumeroLit(String numeroLit) {
		this.numeroLit = numeroLit;
	}


	public String getEtat() {
		return etat;
	}


	public void setEtat(String etat) {
		this.etat = etat;
	}


	public String getType_de_lit() {
		return type_de_lit;
	}


	public void setType_de_lit(String type_de_lit) {
		this.type_de_lit = type_de_lit;
	}


	public Salle getSalle() {
		return salle;
	}


	public void setSalle(Salle salle) {
		this.salle = salle;
	}


	public List<Hospitalisation> getHospitalisations() {
		return hospitalisations;
	}


	public void setHospitalisations(List<Hospitalisation> hospitalisations) {
		this.hospitalisations = hospitalisations;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((etat == null) ? 0 : etat.hashCode());
		result = prime * result + ((hospitalisations == null) ? 0 : hospitalisations.hashCode());
		result = prime * result + idLit;
		result = prime * result + ((numeroLit == null) ? 0 : numeroLit.hashCode());
		result = prime * result + ((salle == null) ? 0 : salle.hashCode());
		result = prime * result + ((type_de_lit == null) ? 0 : type_de_lit.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lit other = (Lit) obj;
		if (etat == null) {
			if (other.etat != null)
				return false;
		} else if (!etat.equals(other.etat))
			return false;
		if (hospitalisations == null) {
			if (other.hospitalisations != null)
				return false;
		} else if (!hospitalisations.equals(other.hospitalisations))
			return false;
		if (idLit != other.idLit)
			return false;
		if (numeroLit != other.numeroLit)
			return false;
		if (salle == null) {
			if (other.salle != null)
				return false;
		} else if (!salle.equals(other.salle))
			return false;
		if (type_de_lit == null) {
			if (other.type_de_lit != null)
				return false;
		} else if (!type_de_lit.equals(other.type_de_lit))
			return false;
		return true;
	}
	
	
	

	
	

	

}
