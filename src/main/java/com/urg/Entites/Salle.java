package com.urg.Entites;

import java.io.Serializable;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "salle", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Salle implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id 
	private int idSalle;
	@Column(unique =true )
    private String nomSalle;
	private String localisation;
    private String typeSalle;

    //private String numero;
   // private int nombre_de_lit;
    //service
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idService", nullable = false)
	private Service service;
	//lit
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "salle")
	private List<Lit> lits;
	
	public Salle() {
	}
	public Salle(int idSalle, String nomSalle, String localisation, String typeSalle,/* int nombre_de_lit,*/
			Service service, List<Lit> lits) {
		super();
		this.idSalle = idSalle;
		this.nomSalle = nomSalle;
		this.localisation = localisation;
		this.typeSalle = typeSalle;
		//this.nombre_de_lit = nombre_de_lit;
		this.service = service;
		this.lits = lits;
	}
	public Salle(int idSalle, String nomSalle, String localisation, String typeSalle,/* int nombre_de_lit,*/
			Service service) {
		super();
		this.idSalle = idSalle;
		this.nomSalle = nomSalle;
		this.localisation = localisation;
		this.typeSalle = typeSalle;
		//this.nombre_de_lit = nombre_de_lit;
		this.service = service;
	}
	public int getIdSalle() {
		return idSalle;
	}
	public void setIdSalle(int idSalle) {
		this.idSalle = idSalle;
	}
	public String getNomSalle() {
		return nomSalle;
	}
	public void setNomSalle(String nomSalle) {
		this.nomSalle = nomSalle;
	}
	public String getLocalisation() {
		return localisation;
	}
	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}
	
	
	public String getTypeSalle() {
		return typeSalle;
	}
	public void setTypeSalle(String typeSalle) {
		this.typeSalle = typeSalle;
	}
	
	/*public int getNombre_de_lit() {
		return nombre_de_lit;
	}
	public void setNombre_de_lit(int nombre_de_lit) {
		this.nombre_de_lit = nombre_de_lit;
	}*/
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
	public List<Lit> getLits() {
		return lits;
	}
	public void setLits(List<Lit> lits) {
		this.lits = lits;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idSalle;
		result = prime * result + ((lits == null) ? 0 : lits.hashCode());
		result = prime * result + ((localisation == null) ? 0 : localisation.hashCode());
		result = prime * result + ((nomSalle == null) ? 0 : nomSalle.hashCode());
		result = prime * result + ((service == null) ? 0 : service.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Salle other = (Salle) obj;
		
		if (idSalle != other.idSalle)
			return false;
		if (lits == null) {
			if (other.lits != null)
				return false;
		} else if (!lits.equals(other.lits))
			return false;
		if (localisation == null) {
			if (other.localisation != null)
				return false;
		} else if (!localisation.equals(other.localisation))
			return false;
		if (nomSalle == null) {
			if (other.nomSalle != null)
				return false;
		} else if (!nomSalle.equals(other.nomSalle))
			return false;
		
		if (service == null) {
			if (other.service != null)
				return false;
		} else if (!service.equals(other.service))
			return false;
		return true;
	}

   

	

}
