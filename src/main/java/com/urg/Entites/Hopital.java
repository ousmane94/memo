package com.urg.Entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Table(name = "hopital", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Hopital implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private int idHopital;
	@Column(unique =true )
	private String nomHopital;
	private int capacite;
//	private int nombre_de_salle;
	//utiliateur
	@OneToMany(mappedBy = "hopital" ,fetch=FetchType.LAZY)
	private List<Personne> utilisateurs;
	//Service
	@OneToMany( mappedBy = "hopital" ,fetch=FetchType.LAZY)
	private List<Service> services;
	
	
	@JsonCreator
	public Hopital(
			@JsonProperty("idHopital") int idHopital,
			@JsonProperty("nomHopital") String nomHopital ,
			@JsonProperty("capacite") int capacite ,
			//@JsonProperty("nombre_de_salle") int nombre_de_salle ,
			@JsonProperty("personne") List<Personne> utilisateurs,
			@JsonProperty("service") List<Service> services)
	{
		this.idHopital = idHopital;
		this.nomHopital = nomHopital;
		this.capacite = capacite;
		//this.nombre_de_salle=nombre_de_salle;
		this.utilisateurs = utilisateurs;
		this.services=services;
	}
	public Hopital() {
		// TODO Auto-generated constructor stub
	}
	public int getIdHopital() {
		return idHopital;
	}
	public void setIdHopital(int idHopital) {
		this.idHopital = idHopital;
	}
	public String getNomHopital() {
		return nomHopital;
	}
	public void setNomHopital(String nomHopital) {
		this.nomHopital = nomHopital;
	}
	public int getCapacite() {
		return capacite;
	}
	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}
	/*public int getNombre_de_salle() {
		return nombre_de_salle;
	}
	public void setNombre_de_salle(int nombre_de_salle) {
		this.nombre_de_salle = nombre_de_salle;
	}*/
	public List<Personne> getUtilisateurs() {
		return utilisateurs;
	}
	public void setUtilisateurs(List<Personne> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}
	public List<Service> getServices() {
		return services;
	}
	public void setServices(List<Service> services) {
		this.services = services;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
