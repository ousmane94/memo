package com.urg.Entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Table(name = "patient", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Patient implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	 private int idPatient;
     private int numerPatient;
     private String nomPatient;
     private String prenomPatient;
     private Date date_naissance_Patient;
     private String sexe;
     private String adresse;
     private String situationFamilliale;
     private int telephone;
     private String nomP_a_prevenir;
     private int telP_a_prevenir;
     private Date dateCreationDossier;
 	 //consultation
 	 @OneToMany(fetch = FetchType.LAZY, mappedBy = "patient")
 	 private List<Consultation> consultations;
	public Patient(int idPatient, int numerPatient, String nomPatient, String prenomPatient,
			Date date_naissance_Patient, String sexe, String adresse, String situationFamilliale, int telephone,
			String nomP_a_prevenir, int telP_a_prevenir, Date dateCreationDossier, List<Consultation> consultations) {
		super();
		this.idPatient = idPatient;
		this.numerPatient = numerPatient;
		this.nomPatient = nomPatient;
		this.prenomPatient = prenomPatient;
		this.date_naissance_Patient = date_naissance_Patient;
		this.sexe = sexe;
		this.adresse = adresse;
		this.situationFamilliale = situationFamilliale;
		this.telephone = telephone;
		this.nomP_a_prevenir = nomP_a_prevenir;
		this.telP_a_prevenir = telP_a_prevenir;
		this.dateCreationDossier = dateCreationDossier;
		this.consultations = consultations;
	}
	public Patient() {
	}
	public int getIdPatient() {
		return idPatient;
	}
	public void setIdPatient(int idPatient) {
		this.idPatient = idPatient;
	}
	public int getNumerPatient() {
		return numerPatient;
	}
	public void setNumerPatient(int numerPatient) {
		this.numerPatient = numerPatient;
	}
	public String getNomPatient() {
		return nomPatient;
	}
	public void setNomPatient(String nomPatient) {
		this.nomPatient = nomPatient;
	}
	public String getPrenomPatient() {
		return prenomPatient;
	}
	public void setPrenomPatient(String prenomPatient) {
		this.prenomPatient = prenomPatient;
	}
	public Date getDate_naissance_Patient() {
		return date_naissance_Patient;
	}
	public void setDate_naissance_Patient(Date date_naissance_Patient) {
		this.date_naissance_Patient = date_naissance_Patient;
	}
	public String getSexe() {
		return sexe;
	}
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getSituationFamilliale() {
		return situationFamilliale;
	}
	public void setSituationFamilliale(String situationFamilliale) {
		this.situationFamilliale = situationFamilliale;
	}
	public int getTelephone() {
		return telephone;
	}
	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}
	public String getNomP_a_prevenir() {
		return nomP_a_prevenir;
	}
	public void setNomP_a_prevenir(String nomP_a_prevenir) {
		this.nomP_a_prevenir = nomP_a_prevenir;
	}
	public int getTelP_a_prevenir() {
		return telP_a_prevenir;
	}
	public void setTelP_a_prevenir(int telP_a_prevenir) {
		this.telP_a_prevenir = telP_a_prevenir;
	}
	public Date getDateCreationDossier() {
		return dateCreationDossier;
	}
	public void setDateCreationDossier(Date dateCreationDossier) {
		this.dateCreationDossier = dateCreationDossier;
	}
	public List<Consultation> getConsultations() {
		return consultations;
	}
	public void setConsultations(List<Consultation> consultations) {
		this.consultations = consultations;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
		result = prime * result + ((consultations == null) ? 0 : consultations.hashCode());
		result = prime * result + ((dateCreationDossier == null) ? 0 : dateCreationDossier.hashCode());
		result = prime * result + ((date_naissance_Patient == null) ? 0 : date_naissance_Patient.hashCode());
		result = prime * result + idPatient;
		result = prime * result + ((nomP_a_prevenir == null) ? 0 : nomP_a_prevenir.hashCode());
		result = prime * result + ((nomPatient == null) ? 0 : nomPatient.hashCode());
		result = prime * result + numerPatient;
		result = prime * result + ((prenomPatient == null) ? 0 : prenomPatient.hashCode());
		result = prime * result + ((sexe == null) ? 0 : sexe.hashCode());
		result = prime * result + ((situationFamilliale == null) ? 0 : situationFamilliale.hashCode());
		result = prime * result + telP_a_prevenir;
		result = prime * result + telephone;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Patient other = (Patient) obj;
		if (adresse == null) {
			if (other.adresse != null)
				return false;
		} else if (!adresse.equals(other.adresse))
			return false;
		if (consultations == null) {
			if (other.consultations != null)
				return false;
		} else if (!consultations.equals(other.consultations))
			return false;
		if (dateCreationDossier == null) {
			if (other.dateCreationDossier != null)
				return false;
		} else if (!dateCreationDossier.equals(other.dateCreationDossier))
			return false;
		if (date_naissance_Patient == null) {
			if (other.date_naissance_Patient != null)
				return false;
		} else if (!date_naissance_Patient.equals(other.date_naissance_Patient))
			return false;
		if (idPatient != other.idPatient)
			return false;
		if (nomP_a_prevenir == null) {
			if (other.nomP_a_prevenir != null)
				return false;
		} else if (!nomP_a_prevenir.equals(other.nomP_a_prevenir))
			return false;
		if (nomPatient == null) {
			if (other.nomPatient != null)
				return false;
		} else if (!nomPatient.equals(other.nomPatient))
			return false;
		if (numerPatient != other.numerPatient)
			return false;
		if (prenomPatient == null) {
			if (other.prenomPatient != null)
				return false;
		} else if (!prenomPatient.equals(other.prenomPatient))
			return false;
		if (sexe == null) {
			if (other.sexe != null)
				return false;
		} else if (!sexe.equals(other.sexe))
			return false;
		if (situationFamilliale == null) {
			if (other.situationFamilliale != null)
				return false;
		} else if (!situationFamilliale.equals(other.situationFamilliale))
			return false;
		if (telP_a_prevenir != other.telP_a_prevenir)
			return false;
		if (telephone != other.telephone)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Patient [idPatient=" + idPatient + ", numerPatient=" + numerPatient + ", nomPatient=" + nomPatient
				+ ", prenomPatient=" + prenomPatient + ", date_naissance_Patient=" + date_naissance_Patient + ", sexe="
				+ sexe + ", adresse=" + adresse + ", situationFamilliale=" + situationFamilliale + ", telephone="
				+ telephone + ", nomP_a_prevenir=" + nomP_a_prevenir + ", telP_a_prevenir=" + telP_a_prevenir
				+ ", dateCreationDossier=" + dateCreationDossier + ", consultations=" + consultations + "]";
	}
 	 
 	 
 	 
 	
     
     
     
     
     
     
     
     

}
