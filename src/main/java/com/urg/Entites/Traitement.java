package com.urg.Entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "traitement", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Traitement implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private int idTraitement;
	private Date dateTraitement;
	//Consultation
	@ManyToOne(fetch = FetchType.LAZY)
 	@JoinColumn(name = "idConsultation", nullable = false)
	private Consultation consultation;


	
	
	

}
