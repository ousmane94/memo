package com.urg.Entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table(name = "specialite", catalog = "urgenceDB")
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Specialite implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private int idSpecialite;
	private String nomSpecialite;
	//utiliateur
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "specialite")
	private List<Medecin> medecins;
	
	public Specialite() {
		
	}
	public Specialite(int idSpecialite, String nomSpecialite, List<Medecin> medecins) {
		super();
		this.idSpecialite = idSpecialite;
		this.nomSpecialite = nomSpecialite;
		this.medecins = medecins;
	}
	public int getIdSpecialite() {
		return idSpecialite;
	}
	public void setIdSpecialite(int idSpecialite) {
		this.idSpecialite = idSpecialite;
	}
	public String getNomSpecialite() {
		return nomSpecialite;
	}
	public void setNomSpecialite(String nomSpecialite) {
		this.nomSpecialite = nomSpecialite;
	}
	public List<Medecin> getMedecins() {
		return medecins;
	}
	public void setMedecins(List<Medecin> medecins) {
		this.medecins = medecins;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	

}
