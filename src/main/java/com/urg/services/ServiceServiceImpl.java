package com.urg.services;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.urg.Entites.Hopital;
import com.urg.Entites.Salle;
import com.urg.Entites.Service;
import com.urg.dao.SalleRepository;
import com.urg.dao.ServiceRepository;


@org.springframework.stereotype.Service
@Transactional
public class ServiceServiceImpl implements ServiceService{
	@SuppressWarnings("rawtypes")
	@Autowired
	private ServiceRepository serviceRepository;
	@Autowired
	private SalleRepository salleRepository;

	@Override //find service
	public Service findService(int idService) {
	
		Service service = serviceRepository.findByIdService(idService);
		if (service!=null) {
			return service;
		}
		return null;
	}
	@Override  //find service by nom
	public Service findServiceByNom(String nomService) {
		System.out.println("get mon service par nom");
		Service service = serviceRepository.findByNomService(nomService);
		if (service!=null) {
			System.out.println("creation de  service par nom");

			Service service2 = new Service();
			service2.setMedecins(null);
			service2.setSalles(null);
			service2.setHopital(null);
			service2.setIdService(service.getIdService());
			service2.setNomService(service.getNomService());
			
			return service2;
			
			
		}else {
			System.out.println("service null");

			return null;

		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Service> getServices() {
		
		List<Service> services = (List<Service>) serviceRepository.findAll();
		if (services==null)
			return null;
		return services;
	}
	@SuppressWarnings("unchecked")
	@Override //modification
	public Service modifierService( Service service) {
		serviceRepository.save(service);
		return service;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Boolean supprimerService(int idService) {
		 Service service =serviceRepository.findByIdService(idService);

			if (service!=null) {
				//liste des service
				   System.out.println("Supression des  salle .....");
				   List<Salle>  mesallList = salleRepository.findByService(service); 
				   Iterator<Salle> it=mesallList.iterator();
				    while(it.hasNext())
				   {
			       Salle delSalle=it.next();
				   salleRepository.delete(delSalle);
				   }
				   serviceRepository.deleteById(idService);
				   return  true;
				
			}else {
				   return  false;

			}		
	}

	@Override
	public Page<Service> findPaginationService(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  Boolean ajouterService(Service service) {
		
		Service ser=serviceRepository.findByNomServiceAndHopital(service.getNomService(),service.getHopital());
		 if (ser==null) {
			 serviceRepository.save(service);
				System.out.println("Superrrrr géniale******************************");
				return true;
		}
			return false;
	}
	@Override
	public List<Service> getserviceHopital(Hopital hopital) {
		
		@SuppressWarnings("unchecked")
		List<Service> services =  serviceRepository.findByHopital(hopital);
		if (services==null)
			return null;
		return services;
	}
		 
}
