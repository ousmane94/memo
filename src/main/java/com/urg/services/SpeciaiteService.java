package com.urg.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.urg.Entites.Specialite;

public interface SpeciaiteService {
	public Boolean ajouterSpecialite(Specialite specialite);
	public Specialite findSpecialite(int idSpecialite);
	public Specialite findByNomSpecialite(String nomService);
	public List<Specialite> getSpecialite();
	public Specialite modifierSpecialite(Specialite specialite);
	public Boolean supprimerSpecialite(int idSpecialite);
    public Page<Specialite> findPaginationService(Pageable pageable);
}
