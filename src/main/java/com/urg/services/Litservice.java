package com.urg.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.urg.Entites.Lit;
public interface Litservice {
	public Boolean ajouterLit(Lit lit);
	public Lit findByIdLits(int idLit);
	
	public Lit findByNumeroLits(String numeroLit);
	public List<Lit> getLits();
	public Lit modifierLit(Lit Lit);
	public Boolean supprimerLit(int idLit);
    public Page<Lit> findPaginationLits(Pageable pageable);
}
