package com.urg.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.urg.Entites.Patient;


public interface PatientService {
	
	public Patient ajouterPatient(Patient patient);
	//public Profil ajouteProfil(Profil profil);
	public Patient lectureByNomPatient(String nomPatient);
    //public void ajouterRoleUser(String username,String nomProfil);
    public List<Patient> findPatient();
	//public List<Produit> geProduits();
	public Patient updeatePatient(int idLong ,Patient patient);
	public void deleteUtilisateur(int idPatient);
    public Page<Patient> findPaginated(Pageable pageable);

}
