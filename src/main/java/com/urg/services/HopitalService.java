package com.urg.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.urg.Entites.Hopital;
import com.urg.Entites.Utilisateur;


public interface HopitalService {
	
	public void ajouterHopital(Hopital hopital);
	public Hopital findHopital(int idHopital);
	public Hopital findHopitalByNom(String nomHopital);
	public List<Hopital> geHopitals();
	public Hopital modifierHopital(int idhopital ,Hopital hopital);
	public Boolean supprimerHopital(int idHopital);
    public Page<Hopital> findPaginationHopital(Pageable pageable);
    

}
