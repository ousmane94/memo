package com.urg.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.urg.Entites.Patient;
import com.urg.Entites.Utilisateur;
import com.urg.dao.PatientRepository;

public class PatientServiceImpl  implements PatientService {
	
	@Autowired
	private PatientRepository patientRepository;
	
	
	@Override
	public Patient ajouterPatient(Patient patient) {
		Patient patient1 =patientRepository.findByNumerPatient(patient.getNumerPatient());
		if(patient1!=null) throw new RuntimeException("Patient existe deja");
		Patient patient2 = new Patient();
		patient2.setAdresse(patient.getAdresse());
		patient2.setNomPatient(patient.getNomPatient());
		patient2.setDate_naissance_Patient(patient.getDate_naissance_Patient());
		patient2.setDateCreationDossier(new Date());
		patient2.setPrenomPatient(patient.getPrenomPatient());
		patient2.setNumerPatient((patient.getNumerPatient()));
		patient2.setTelephone(patient.getTelephone());
		patient2.setSexe(patient.getSexe());
		patient2.setSituationFamilliale(patient.getSituationFamilliale());
		patient2.setIdPatient(patient.getIdPatient());
		patient2.setNomP_a_prevenir(patient.getNomP_a_prevenir());
		patient2.setTelP_a_prevenir(patient.getTelP_a_prevenir());
		patientRepository.save(patient2);
		return patient2;
	}

	@Override
	public Patient lectureByNomPatient(String nomPatient) {
		return patientRepository.findByNomPatient(nomPatient) ;
	}

	@Override
	public List<Patient> findPatient() {
		
		List<Patient> patients = (List<Patient>) patientRepository.findAll();
		if (patients==null)
			return null;
		return patients;
	}

	@Override
	public Patient updeatePatient(int idLong, Patient patient) {
		patient.setIdPatient(idLong);
		return patientRepository.save(patient);
	}

	@Override
	public void deleteUtilisateur(int idPatient) {
		patientRepository.deleteById(idPatient);
		
	}

	@Override
	public Page<Patient> findPaginated(Pageable pageable) {
		
		return patientRepository.finPage(pageable);

	}

}
