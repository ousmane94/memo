package com.urg.services;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.urg.Entites.Medecin;
import com.urg.Entites.Specialite;
import com.urg.dao.MedecinRepository;
import com.urg.dao.SpecialiteRepository;

@Service
@Transactional
public class SpecialiteServiceImpl  implements SpeciaiteService{
	

	@Autowired
	private SpecialiteRepository specialiteRepository;
	@Autowired
	private MedecinRepository medecinRepository;

	@Override
	public Boolean ajouterSpecialite(Specialite specialite) {
	Specialite ser=specialiteRepository.findByNomSpecialite(specialite.getNomSpecialite());
	 if (ser==null) {
		 specialiteRepository.save(specialite);
			System.out.println("Superrrrr géniale******************************");
			return true;
	}
		return false;		
	}

	@Override
	public Specialite findSpecialite(int idSpecialite) {
		Specialite specialite = specialiteRepository.findByIdSpecialite(idSpecialite);
		if (specialite!=null) {
			return specialite;
		}
		return null;
	}

	@Override
	public Specialite findByNomSpecialite(String nomSpecialite) {
		System.out.println("get mon Specialite par nom");
		Specialite specialite = specialiteRepository.findByNomSpecialite(nomSpecialite);
		if (specialite!=null) {
			Specialite specialite2 = new Specialite();
			specialite2.setIdSpecialite(specialite.getIdSpecialite());
			specialite2.setNomSpecialite(specialite.getNomSpecialite());
			specialite2.setMedecins(null);
			return specialite2;	
			
		}else {
			return null;

		}
	}

	@Override
	public List<Specialite> getSpecialite() {
		List<Specialite> services = (List<Specialite>) specialiteRepository.findAll();
		if (services==null)
			return null;
		return services;
	}

	@Override
	public Specialite modifierSpecialite(Specialite specialite) {
		return specialiteRepository.save(specialite);
	}

	@Override
	public Boolean supprimerSpecialite(int idSpecialite) {
		
		
		 Specialite service =specialiteRepository.findByIdSpecialite(idSpecialite);

			if (service!=null) {
				//liste des specialite
				   System.out.println("Supression des  Specialite .....");
				   List<Medecin>  mesallList = medecinRepository.findBySpecialite(service); 
				   Iterator<Medecin> it=mesallList.iterator();
				    while(it.hasNext())
				   {
			       Medecin delSalle=it.next();
			       delSalle.setSpecialite(null);
				   medecinRepository.save(delSalle);
				   }
				   specialiteRepository.deleteById(idSpecialite);
				   return  true;
				
			}else {
				   return  false;

			}
		}

	@Override
	public Page<Specialite> findPaginationService(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}
