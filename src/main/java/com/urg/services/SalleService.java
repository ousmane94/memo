package com.urg.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.urg.Entites.Salle;

public interface SalleService {
	
	public Boolean ajouterSalle(Salle salle);
	public Salle findByIdSalle(int idSalle);
	public Salle findByNomSalle (String nomSalle);
	public List<Salle> getSalles();
	public Salle modifierSalle(Salle salle);
	public Boolean supprimerSallle(int idService);
    public Page<Salle> findPaginationSalle(Pageable pageable);

}
