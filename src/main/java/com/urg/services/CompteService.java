 package com.urg.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.urg.Entites.Medecin;
import com.urg.Entites.Personne;
import com.urg.Entites.Utilisateur;

public interface CompteService {
	
	public Utilisateur ajouterUtilisateur(Utilisateur utilisateur);
	//public Profil ajouteProfil(Profil profil);
	public Utilisateur lectureByUsername(String username);
    //public void ajouterRoleUser(String username,String nomProfil);
    public List<Utilisateur> findUtilisateursl();
	//public List<Produit> geProduits();
	public Utilisateur updeateUtilisateur(int idLong ,Utilisateur utilisateur);
	public void deleteUtilisateur(int idproduit);
    public Page<Utilisateur> findPaginated(Pageable pageable);
	public Medecin connectByUsername(String username);



}
