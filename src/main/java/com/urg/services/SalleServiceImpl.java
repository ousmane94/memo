package com.urg.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.urg.Entites.Salle;
import com.urg.dao.SalleRepository;
@Service
@Transactional
public class SalleServiceImpl implements SalleService {
	
	@Autowired
	private SalleRepository salleRepository;

	@Override
	public Boolean ajouterSalle(Salle salle) {
		    salleRepository.save(salle);
		    return true;
	}

	@Override
	public Salle findByIdSalle(int idSalle) {

		Salle salle = salleRepository.findByIdSalle(idSalle);
		if (salle!=null) {
			return salle;
		}
		return null;
	}

	@Override
	public Salle findByNomSalle(String nomsalle) {
		  Salle salle = salleRepository.findByNomSalle(nomsalle);
		  if (salle!=null) {
			  Salle salle2 = new Salle();
			  salle2.setIdSalle(salle.getIdSalle());
			  salle2.setLocalisation(salle.getLocalisation());
			  salle2.setNomSalle(salle.getNomSalle());
			  salle2.setTypeSalle(salle.getTypeSalle());
			  salle2.setService(null);
			 return salle2;
		}else {
			return null;

		}
	}

	@Override
	public List<Salle> getSalles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Salle modifierSalle(Salle salle) {
		salleRepository.save(salle);
		return salle;
	}

	@Override
	public Boolean supprimerSallle(int  idSalle) {
		salleRepository.deleteById(idSalle);
		return true;
		
	}

	@Override
	public Page<Salle> findPaginationSalle(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}
