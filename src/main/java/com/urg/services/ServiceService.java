package com.urg.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.urg.Entites.Hopital;
import com.urg.Entites.Service;

public interface ServiceService {
	public Boolean ajouterService(Service service);
	public Service findService(int idService);
	public Service findServiceByNom(String nomService);
	public List<Service> getServices();
	public Service modifierService(Service Service);
	public Boolean supprimerService(int idService);
    public Page<Service> findPaginationService(Pageable pageable);
    //ajoute 
    public List<Service> getserviceHopital(Hopital hopital);
    

}
