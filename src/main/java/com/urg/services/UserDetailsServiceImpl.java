package com.urg.services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.urg.Entites.Personne;
import com.urg.Entites.Utilisateur;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	CompteService accountService;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Personne user=accountService.lectureByUsername(username);
		if(user==null) throw new UsernameNotFoundException("invalide utilisateur");
		Collection<GrantedAuthority> authorities=new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority(user.getProfils()));
		return new User(user.getUsername(),user.getPassword(),authorities);
	}

}
