package com.urg.services;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.urg.Entites.Hopital;
import com.urg.Entites.Utilisateur;
import com.urg.dao.HopitalRepository;
import com.urg.dao.ServiceRepository;
import com.urg.dao.UtilisateurRepository;

@Service
@Transactional
public class HopitalServiceImpl implements HopitalService {
	@Autowired
	private HopitalRepository hopitalRepository;
	@SuppressWarnings("rawtypes")
	@Autowired
	private ServiceRepository serviceRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Override
	public void ajouterHopital(Hopital hopital) {
		hopitalRepository.save(hopital);
		
	}

	@Override //trouver un hopital
	public Hopital findHopital(int idHopital) {
		return hopitalRepository.findByIdHopital(idHopital);
	}

	@Override //gethopital
	public List<Hopital> geHopitals() {
		List<Hopital> hopitals = (List<Hopital>) hopitalRepository.findAll();
		if (hopitals==null)
			return null;
		return hopitals;
	}

	@Override //modifier hopital
	public Hopital modifierHopital(int idhopital, Hopital hopital) {
   		hopital.setIdHopital(idhopital);
		return hopitalRepository.save(hopital);
	}

	@Override //supprimer hopital
	public Boolean supprimerHopital(int idHopital) {
		   Hopital hopital =hopitalRepository.findByIdHopital(idHopital);

			if (hopital!=null) {
				//liste des service
				   System.out.println("Supression des  Services .....");
				   List<com.urg.Entites.Service>  meservList = serviceRepository.findByHopital(hopital);
				   Iterator<com.urg.Entites.Service> it=meservList.iterator();
				    while(it.hasNext())
				   {
			       com.urg.Entites.Service delService=it.next();

				   serviceRepository.delete(delService);
				   }
				   List<Utilisateur>  userList = utilisateurRepository.findByHopital(hopital);
				   Iterator<Utilisateur> it2=userList.iterator();
				   while(it.hasNext())
				   {
					   Utilisateur  delUser=it2.next();
					   utilisateurRepository.delete(delUser);

				   }

				   hopitalRepository.deleteById(idHopital);
				   return  true;
				
			}else {
				   return  false;

			}		
		
	}

	@Override //pagination
	public Page<Hopital> findPaginationHopital(Pageable pageable) {
		
		return hopitalRepository.finPage(pageable);
	}

	@Override //trouver Hopital par nom
	public Hopital findHopitalByNom(String nomHopital) {
		Hopital hopital = hopitalRepository.findByNomHopital(nomHopital);
		if(hopital==null) throw new RuntimeException("Hopital introuvable ");
		return hopital;
	}



}
