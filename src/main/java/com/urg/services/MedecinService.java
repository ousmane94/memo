package com.urg.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.urg.Entites.Medecin;

public interface MedecinService {
	
	public Medecin ajouterMedecin(Medecin medecin);
	//public Profil ajouteProfil(Profil profil);
	public Medecin lectureByUsername(String username);
    //public void ajouterRoleUser(String username,String nomProfil);
    public List<Medecin> findMedecinsl();
	//public List<Produit> geProduits();
	public Medecin updeateMedecin(int idLong ,Medecin medecin);
	public void deleteMedecin(int idMedecin);
    public Page<Medecin> findPaginated(Pageable pageable);

}
