package com.urg.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.urg.Entites.Medecin;
import com.urg.Entites.Personne;
import com.urg.Entites.Utilisateur;
import com.urg.dao.PersonneRepository;
import com.urg.dao.UtilisateurRepository;

@Service
@Transactional
public class CompteServiceImpl implements CompteService {
	
	
	//@Autowired
	//private ProfilRepository profilRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private PersonneRepository PersonneRepository;
	
	
	//@Autowired
 //BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public Utilisateur ajouterUtilisateur(Utilisateur utilisateur) {
		
		
		Personne user1 = utilisateurRepository.findByUsername(utilisateur.getUsername());
		if(user1!=null) throw new RuntimeException("utilisateur existe deja");
		Utilisateur user2 = new Utilisateur();
		String  iduser1=""+((int) (Math.random()*(9-0)))
				+((int) (Math.random()*(9-0)))
				+((int) (Math.random()*(9-0)));
     Integer uss1=java.lang.Integer.valueOf(iduser1);
		user2.setAdresse(utilisateur.getAdresse());
		user2.setPrenom(utilisateur.getPrenom());
		user2.setEmail(utilisateur.getEmail());
		user2.setGenre(utilisateur.getGenre());
		user2.setNom(utilisateur.getNom());
		user2.setPassword(bCryptPasswordEncoder.encode(utilisateur.getPassword()));
		user2.setTelephone(utilisateur.getTelephone());
		user2.setHopital(utilisateur.getHopital());
		user2.setUsername(utilisateur.getNom()+utilisateur.getPrenom()+"@"+utilisateur.getHopital().getNomHopital()+".sn");
		user2.setProfils(utilisateur.getProfils());
		user2.setIdPersonne(uss1);
		utilisateurRepository.save(user2);
		//ajouterRoleUser(confirmUsername, "user");
		return user2;
	}
	/*
	 * @Override public Profil ajouteProfil(Profil profil) { return
	 * profilRepository.save(profil); }
	 */
	@Override
	public Utilisateur lectureByUsername(String username) {
		return utilisateurRepository.findByUsername(username);
	}

	@Override
	public List<Utilisateur> findUtilisateursl() {
		List<Utilisateur> utilisateurs =  utilisateurRepository.findlisteUtilisateurs();
		if (utilisateurs==null)
			return null;
		return utilisateurs;
	}

	@Override
	public void deleteUtilisateur(int idutilisateur) {
		utilisateurRepository.deleteById(idutilisateur);
	}

	@Override
	public Page<Utilisateur> findPaginated(Pageable pageable) {
		
		return utilisateurRepository.finPage(pageable);

	}

	@Override
	public Utilisateur updeateUtilisateur(int idLong, Utilisateur utilisateur) {
		// TODO Auto-generated method stub
		utilisateur.setIdPersonne(idLong);
		return utilisateurRepository.save(utilisateur);		
	}
	@Override
	public Medecin connectByUsername(String username) {
		// TODO Auto-generated method stub
		return (Medecin) PersonneRepository.findByUsername(username);

	}
	

	

	


}
