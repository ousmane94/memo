package com.urg.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.urg.Entites.Medecin;
import com.urg.dao.MedecinRepository;
@Service
@Transactional
public class MedecinServiceImpl  implements MedecinService{
	@Autowired
	private MedecinRepository medecinRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public Medecin ajouterMedecin(Medecin medecin) {
		Medecin user1 =medecinRepository.findBymatricule(medecin.getMatricule());
		if(user1!=null) throw new RuntimeException("Medecin existe deja");
		Medecin user2 = new Medecin();
		String  iduser1=""+((int) (Math.random()*(9-0)))
				+((int) (Math.random()*(9-0)))
				+((int) (Math.random()*(9-0)));
        Integer uss1=java.lang.Integer.valueOf(iduser1);
		user2.setAdresse(medecin.getAdresse());
		user2.setPrenom(medecin.getPrenom());
		user2.setEmail(medecin.getEmail());
		user2.setGenre(medecin.getGenre());
		user2.setNom(medecin.getNom());
		user2.setSpecialite(medecin.getSpecialite());
		user2.setService(medecin.getService());
		user2.setMatricule(medecin.getHopital().getNomHopital()+uss1);
		user2.setPassword(null);
		user2.setTelephone(medecin.getTelephone());
		user2.setHopital(medecin.getHopital());
		user2.setUsername(medecin.getNom()+medecin.getPrenom()+"@"+medecin.getHopital().getNomHopital()+".sn");
		//user2.setUsername(null);
		user2.setProfils(null);
		user2.setIdPersonne(uss1);
		medecinRepository.save(user2);
		//ajouterRoleUser(confirmUsername, "user");
		return user2;
	}

	@Override
	public Medecin lectureByUsername(String username) {
		return medecinRepository.findBymatricule(username);
	}

	@Override
	public List<Medecin> findMedecinsl() {
		List<Medecin> medecin = (List<Medecin>) medecinRepository.findAll();
		if (medecin==null)
			return null;
		return medecin;
	}

	@Override
	public Medecin updeateMedecin(int idLong, Medecin medecin) {
		medecin.setIdPersonne(idLong);
		return medecinRepository.save(medecin);
	}

	@Override
	public void deleteMedecin(int idMedecin) {
		medecinRepository.deleteById(idMedecin);
		
	}

	@Override
	public Page<Medecin> findPaginated(Pageable pageable) {
		return medecinRepository.finPage(pageable);

	}

}
