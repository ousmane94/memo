package com.urg.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.urg.Entites.Lit;
import com.urg.dao.LitRepository;
import com.urg.dao.SalleRepository;

@Service
@Transactional
public class LitserviceImpl implements Litservice {

	@Autowired
	private LitRepository litRepository;
	@Autowired
	private SalleRepository salleRepository;
	
	
	@Override
	public Boolean ajouterLit(Lit lit) {
		Lit ser=litRepository.findByNumeroLitAndSalle(lit.getNumeroLit(),lit.getSalle());
		 if (ser==null) {
			 litRepository.save(lit);
				System.out.println("Superrrrr géniale******************************");
				return true;
		}
			return false;
	}

	@Override
	public Lit findByIdLits(int idLit) {
		Lit lit = litRepository.findByIdLit(idLit);
		if (lit!=null) {
			return lit;
		}
		return null;
	}

	@Override
	public Lit findByNumeroLits(String numeroLit) {
		System.out.println("get numero lit ");
		Lit lit = litRepository.findByNumeroLit(numeroLit);
		if (lit!=null) {
			Lit lit2 = new Lit();
			lit2.setIdLit(lit.getIdLit());
			lit2.setNumeroLit(lit.getNumeroLit());
			lit2.setEtat(lit.getEtat());
			lit2.setType_de_lit(lit.getType_de_lit());
			lit2.setHospitalisations(null);
			lit2.setSalle(null);
			return lit2;
			
			
		}else {
			return null;

		}
	}

	@Override
	public List<Lit> getLits() {
		List<Lit> lit = (List<Lit>) litRepository.findAll();
		if (lit==null)
			return null;
		return lit;
	}

	@Override
	public Lit modifierLit(Lit lit) {
		return litRepository.save(lit);
	}

	@Override
	public Boolean supprimerLit(int idLit) {
		Lit lit =litRepository.findByIdLit(idLit);

		if (lit!=null) {
			/*liste des service
			   System.out.println("Supression des Hospitalisation   .....");
			   List<Hospitalisation>  mesallList = hospitalisationRepository.findByLit(lit); 
			   Iterator<Hospitalisation> it=mesallList.iterator();
			    while(it.hasNext())
			   {
		       Hospitalisation delSalle=it.next();

			   hospitalisationRepository.delete(delSalle);
			   }
			  /* List<Medecin>  userList = MedecinRepository.findByService(service);
			   Iterator<Medecin> it2=userList.iterator();
			   while(it.hasNext())
			   {
				   Medecin  delUser=it2.next();
				   medecinRepository.delete(delUser);

			   }*/

			   litRepository.deleteById(idLit);
			   return  true;
			
		}else {
			   return  false;

		}		
	}

	@Override
	public Page<Lit> findPaginationLits(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}
