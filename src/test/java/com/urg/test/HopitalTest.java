package com.urg.test;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.urg.Entites.Hopital;
import com.urg.Entites.Medecin;
import com.urg.Entites.Salle;
import com.urg.Entites.Service;
import com.urg.Entites.Utilisateur;
import com.urg.dao.HopitalRepository;
import com.urg.dao.SalleRepository;
import com.urg.dao.ServiceRepository;
import com.urg.dao.UtilisateurRepository;
import com.urg.services.CompteService;
import com.urg.services.CompteServiceImpl;
import com.urg.services.HopitalService;
import com.urg.services.SalleService;
import com.urg.services.ServiceService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HopitalTest  extends Assert{
	@Autowired
	CompteService compteService;
	@Autowired
	private HopitalService hopitalService;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private HopitalRepository hopitalRepository;
	@Autowired
	private ServiceRepository serviceService;
	@Autowired
	private ServiceService serve;
	@Autowired
	private SalleRepository salleRepository;
	@Autowired
	private SalleService salleService;
	@Autowired
	private CompteServiceImpl accountService;
	@Test
	public void contextLoads() {
		System.out.println("creation hopital");
		String  idhopital=""+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)));
	    Integer id=java.lang.Integer.valueOf(idhopital);        
		Hopital hopital = new Hopital();
		hopital.setIdHopital(id);
		hopital.setNomHopital("HDJ");
		hopital.setCapacite(150);
		hopital.setServices(null);
		hopital.setUtilisateurs(null);
		hopitalService.ajouterHopital(hopital);

		System.out.println("*************creation service*******************");
		String  idser=""+((int) (Math.random()*(9-0)))
					+((int) (Math.random()*(9-0)));
	    Integer id3=java.lang.Integer.valueOf(idser);   
	    Hopital hopital22=hopitalService.findHopital(id);
		Service service = new Service();
		service.setIdService(id3);
		service.setHopital(hopital22);
		service.setMedecins(null);
		service.setNomService("HUMINO");
		service.setSalles(null);
		Service ser=serviceService.findByNomServiceAndHopital("HUMINO",hopital22);
		 if (ser==null) {
			 serviceService.save(service);
				System.out.println("Superrrrr géniale******************************");

		}
		System.out.println("xxxxxxxxxxxxxxxxxxxFinxxxxxxxxxxxxxxxxxx");
		System.out.println("*************creation Salle*******************");
		Salle salle = new  Salle();
		salle.setIdSalle(id3);
		salle.setTypeSalle("libre");
		salle.setLocalisation("site");
		Service  service2 =serviceService.findByIdService(service.getIdService());
		salle.setService(service2);
        String num =service.getHopital().getNomHopital();
		salle.setNomSalle(num);
        salle.setLits(null);
		System.out.println("^^^^^"+salle.getIdSalle());
		salleRepository.save(salle);
		System.out.println("*************creation salle2*******************");
		Salle salle2 = new  Salle();
		salle2.setIdSalle(id);
		salle2.setTypeSalle("occupe");
		salle2.setLocalisation("site");
		Service  service22 =serviceService.findByIdService(service2.getIdService());
		salle.setService(service22);
        String num2 =service.getHopital().getNomHopital()+""+service2.getNomService()+""+idser;
       if (num2!=null) {
    	   salle2.setNomSalle(num2);
           salle2.setLits(null);
   		 // salleRepository.save(salle2);
		
	}
       System.out.println("fin");
       Utilisateur utilisateur = new Utilisateur();
       utilisateur.setAdresse("dakar");
       utilisateur.setEmail("gmail.gn");
       utilisateur.setGenre("masculin");
       utilisateur.setHopital(hopital22);
       utilisateur.setNom("barry");
       utilisateur.setPassword("1234");
       utilisateur.setPrenom("ousmane");
       utilisateur.setProfils("admin");
       utilisateur.setTelephone(773344254);
       utilisateur.setUsername("azert");  
	   Utilisateur newUtil= accountService.ajouterUtilisateur(utilisateur);

       
       

	}
	

}
 